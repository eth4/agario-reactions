# README #

This is a README file for my work Agar.io Reactions.

### What is this repository for? ###

This repository is for a Ruby file that will generate how the player would react via input of name.

### How do I get set up? ###

Setup
1. Install Ruby via https://www.ruby-lang.org/
2. Unzip the repository zip this file came in.
3. Find a desired place for 'agario-reactions.rb'. This is the Ruby file.

Configuration
As of this version, there is no ability to configure anything inside. 
Neither have I put comments in the file from my remembering. It is just for use, not a tool exactly.

### Contribution guidelines ###

I have no clue what you shouldn't do cause I only know a bit about Github (even though this is BitBucket) and I am only clear about files, commits, and issues in that.

### Who do I talk to? ###

Send an email at me: ethan.tonedpyro@gmail.com